export default function () {
  return [
    { title: 'Javascript: The Good Parts', pages: 11 },
    { title: 'Harry Potter', pages: 141 },
    { title: 'Eloquent Javascript', pages: 92 },
    { title: 'Javascript Design Patterns', pages: 852 }
  ]
}
